import time
import os
import fnmatch


count_barcode = 0
count_laser = 0
count_error_invalid = 0
count_error_repeat = 0
error_margin = 0


def main():

    try:
        counter_struct_read("./queue/captures/")

    except FileNotFoundError:
        print("Counter config file not found, resetting count!")

        with open('./conf/counter.conf', 'w') as f:
            counter_struct_write(f)

    count()


def counter_struct_write(file):
    file.write("COUNT_BARCODE\t" + str(count_barcode) + "\n")
    file.write("COUNT_LASER\t" + str(count_laser) + "\n")
    file.write("COUNT_ERROR_INVALID\t" + str(count_error_invalid) + "\n")
    file.write("COUNT_ERROR_REPEAT\t" + str(count_error_repeat) + "\n")
    file.write("ERROR_MARGIN\t" + str(error_margin) + "\n")


def counter_struct_read(file):
    with open('./conf/counter.conf', 'r') as f:
        for line in f:
            key, value = line.rsplit(None, 1)

            if "COUNT_BARCODE" in key:
                count_barcode = value
            elif "COUNT_LASER" in key:
                count_laser = value
            elif "COUNT_ERROR_INVALID" in key:
                count_error_invalid = value
            elif "COUNT_ERROR_REPEAT" in key:
                count_error_repeat = value
            elif "ERROR_MARGIN" in key:
                error_margin = key


def count():
    global count_barcode
    global count_laser
    global count_error_repeat

    while True:
        serials = find('*', './queue/counter/')

        if serials:
            for serial in serials:
                f = open(serial)
                print(f.read())
                count_barcode = count_barcode + 1
                while(f.read()):
                    count_error_repeat = count_error_repeat + 1
                f.close()
                os.remove(serial)

        else:
            print("No image found!")
            time.sleep(1)


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


if __name__ == '__main__':
    main()
