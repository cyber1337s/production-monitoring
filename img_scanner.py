# Used to scan the images in the specified directory.
# Scripts run until all .jpg files in the directory is scanned
# Image file is deleted after scan


from pyzbar import pyzbar
import os
import time
import fnmatch
import cv2
import threading


line = 0


def main():

	while True:
		img_captures = find('*.jpg', './queue/captures/')

		if img_captures:
			for image in img_captures:
				scan(image)
		else:
			print("No image found!");
			time.sleep(1)


def find(pattern, path):
	result = []
	for root, dirs, files in os.walk(path):
		for name in files:
			if fnmatch.fnmatch(name, pattern):
				result.append(os.path.join(root, name))
	return result


def scan(img_name):
	image = cv2.imread(img_name)

	barcodes = pyzbar.decode(image)

	for barcode in barcodes:
		(x, y, w, h) = barcode.rect
		cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

		barcodeData = barcode.data.decode("utf-8")
		barcodeType = barcode.type

		text = "{} ({})".format(barcodeData, barcodeType)
		cv2.putText(image, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
			0.5, (0, 0, 255), 2)

		threading.Thread(target=save, args=[barcodeData]).start()
		print("[INFO] Found {} barcode: {}".format(barcodeType, barcodeData))

	cv2.imshow("Image", image)
	os.remove(img_name)


def save(code):
	open('./queue/counter/'+str(code), 'a').write(code + "\n")


if __name__ == '__main__':
	main()
