# Script for capturing images every specified interval.
# Image is saved in the disk. Scanner script is called for processing.

import cv2
import threading
from time import sleep
import itertools

interval = 0.1


def main():
    camera = cv2.VideoCapture(0)

    for c in itertools.count(1):
        thread_capture = threading.Thread(target=capture, args=[camera, c])
        thread_capture.start()
        sleep(interval)


def capture(camera, pic_id):
    re, image = camera.read()
    cv2.imwrite('./queue/captures/cap'+str(pic_id)+'.jpg', image)


if __name__ == "__main__":
    main()
